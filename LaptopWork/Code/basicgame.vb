
Function InitState(playerTypes, boardSize, scoringType)
    Dim gameState(boardSize,boardSize-1)

    Private Const middlePiece = 0
    Private Const topEdge = 1
    Private Const leftEdge = 2
    Private Const rightEdge = 3
    Private Const bottomEdge = 4
    Private Const leftCorner = 5
    Private Const rightCorner = 6
    Private Const botLeftCorner = 7
    Private Const botRightCorner = 8

    gameState(0,0) = boardSize
    gameState(0,1) = playerTypes(0)
    gameState(0,2) = playerTypes(1)
    gameState(0,3) = scoringType

    '0 is middle piece, 1 is top edge, 2 is left, 3 is right, 4 is bottom, 5 is middle piece, 6 is left corner, 7 is right corner, 8 is bot left corner, 9 is bot right corner.

    For Integer i = 1 To boardSize Step 1
        For Integer k = 0 To boardSize-1 Step 1

            If (i = 1 Or i = boardSize)
                
                If (i = 1)
                    Select Case k
                        Case 0
                            gameState(i,k) = 5
                        Case 1 To boardSize-2
                            gameState(i,k) = 1
                        Case boardSize-1
                            gameState(i,k) = 6
                    End Select
                End If

                If (i = boardSize)
                    Select Case k
                        Case 0
                            gameState(i,k) = 7
                        Case 1 To boardSize-2
                            gameState(i,k) = 4
                        Case boardSize-1
                            gameState(i,k) = 8
                    End Select
                End If
            
            Else
                Select Case k
                    Case 0
                        gameState(i,k) = 2
                    Case 1 To boardSize-2
                        gameState(i,k) = 0
                    Case boardSize-1
                        gameState(i,k) = 3
                    Case Else
                        gameState(i,k) = 0
                    End Select

            End If


        Next k

    Next i

End Function
