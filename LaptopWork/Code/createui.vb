
'New Grid Size 513 as it is very nice

Const gridSize As Integer = 513

Dim gameState(19,18) As Integer

gameState(0,0) = 19

Dim Boxes(gameState(0,0)-1, gameState(0,0)-1) As Picturebox

Dim appPath As String = Application.StartupPath()

Public Sub generateUI(gameState As Integer)

    For i As Integer = 1 To gameState(0,0)

        For k As Integer = 0 To gameState(0,0)-1

            Boxes(i-1,k).Image = System.Drawing.Image.FromFile(appPath & CStr(gameState(i,k)) & ".png")
            Boxes(i-1,k).Size = New System.Drawing.Size(gridSize/gameState(0,0), gridSize/gameState(0,0))
            Boxes(i-1,k).SizeMode = PictureBoxSizeMode.StretchImage


End Sub
